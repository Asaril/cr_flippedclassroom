#include "pxt.h"

PN_(Developing `class NamedValue` (Type Conversion Test));

#include "NamedValue.h"

int main() {
    NamedValue t{"verylongheight", 12};
    PX_("verylongheight", t.name());
    PX_("12", t.value());
    PX_("verylongheight: 12", t);
    t = -1;                
    PX_("verylongheight", t.name());
    PX_("-1", t.value());
    PX_("verylongheight: -1", t);
    t = 0;
    PX_("verylongheight: 0", t);
    NamedValue u{"width", 20};
    NamedValue v{"area", 100};
    t = v / u;
    PX_("verylongheight: 5", t);
    PX_("width: 20", u);
    PX_("area: 100", v);
    u += 10;
    v = t * u;
    PX_("verylongheight: 5", t);
    PX_("width: 30", u);
    PX_("area: 150", v);

    t -= 2;
    PX_("verylongheight: 3", t);
    t *= 4;
    PX_("verylongheight: 12", t);
    t /= 6;
    PX_("verylongheight: 2", t);

    PX_("verylongheight: 7", t += 5);
    PX_("verylongheight: 5", t -= 2);
}

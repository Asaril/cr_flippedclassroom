#ifndef VF_ARRAY_H
#define VF_ARRAY_H

#include <cstddef> // std::size_t
#include <iterator>

template<typename T, std::size_t N>
class vf_array {
private:
    T data[N];
    T* filled{&data[0]};
public:

    class iterator: std::iterator<std::random_access_iterator_tag, T> {
        friend class vf_array;

        T* cursor_;
        iterator(T* cursor): cursor_{cursor} {}
        iterator(): cursor_{nullptr} {}

        public:
            T& operator * () { return *cursor_; }
            iterator& operator ++(int) { cursor_++; return *this;}
            iterator& operator --(int) { cursor_--; return *this;}
            bool operator == (const iterator& r) {return cursor_ == r.cursor_;}
    };

    auto max_size() const { return N; }

    void push_back(const T& e) { /*if (size() < N)*/ *filled++ = e;}
    T& pop_back() { /*if (!empty())*/ return *filled--; }

    T& front() { return data[0]; }
    T& back() { return filled[-1]; }
    T& at(std::size_t idx) { return data[idx]; }
    T& operator[](std::size_t idx) { return at[idx]; }

    auto size() const { return filled-data;}
    auto empty() const { return filled==data; }

    iterator end() { return iterator(filled); }
    iterator begin() { return iterator(data); }

};

#endif // VF_ARRAY_H

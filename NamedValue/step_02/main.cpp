#include "pxt.h"

PN_(Developing `class NamedValue` (Constructor Test));

#include "NamedValue.h"

int main() {
    NamedValue t{"height", 12};
   PX_("height", t.name());
   PX_("12", t.value());
}

#ifndef VF_ARRAY_H
#define VF_ARRAY_H

#include <cstddef> // std::size_t

template<typename T, std::size_t N>
class vf_array {
private:
    T data[N];
    T* filled{&data[0]};

    template<typename TI>
    class base_iterator: public std::iterator<std::random_access_iterator_tag, T> {
        friend class vf_array;

        base_iterator(TI& cursor): cursor_{&cursor} {}
        base_iterator(): cursor_{nullptr} {}

        protected:
        TI* cursor_;

        public:
            TI& operator *() const { return *cursor_; }

            base_iterator& operator ++(int) { cursor_++; return *this;}
            base_iterator& operator --(int) { cursor_--; return *this;}

            base_iterator& operator +=(std::size_t r) { cursor_+=r; return *this;}
            base_iterator& operator -=(std::size_t r) { cursor_-=r; return *this;}

            base_iterator operator +(std::size_t r) { return base_iterator{cursor_ + r}; }
            std::size_t operator -(const base_iterator& r) { return cursor_ - r.cursor_ ; }


            bool operator == (const base_iterator& r) {return cursor_ == r.cursor_;}
    };

public:

    using const_iterator = base_iterator<const T>;
    using iterator = base_iterator<T>;

    auto max_size() const { return N; }
    void push_back(T const& e) { *filled++ = e; }
    T& back() { return filled[-1]; }
    std::size_t size() const { return filled - &data[0]; }
    auto empty() const { return (size() == 0); }

    iterator end() { return iterator(*filled); }
    iterator begin() { return iterator(data[0]); }

    const_iterator cend() const { return const_iterator(*filled); }
    const_iterator cbegin() const { return const_iterator(data[0]); }

};


#include <iostream>

template<typename T, std::size_t N>
std::ostream& operator<<(std::ostream& lhs, const typename vf_array<T, N>::const_iterator& rhs) {
    lhs << "const_iterator: " << *rhs;
    return lhs;
}

template<typename T, std::size_t N>
std::ostream& operator<<(std::ostream& lhs, const typename vf_array<T, N>::iterator& rhs) {
    lhs << "iterator: " << *rhs;
    return lhs;
}

#endif // VF_ARRAY_H

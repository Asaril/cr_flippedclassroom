#ifndef NamedValue_H
#define NamedValue_H

#include <cstring>
#include <iostream>

class NamedValue {
    char const* name_;
    int value_;
public:
    NamedValue(char const* name, int value = 0)
        : value_{value}, name_{strdup(name)} {
            std::cout << "ctor" << std::endl;
    }

    ~NamedValue() {
            std::cout << "dtor" << std::endl;
        delete[] name_;
    }

    // other options to set `value_` per default to zero were
    // (2) constructor with only a single argument delegating
    // to the work to the one above:
    // NamedValue(char const* name) : NamedValue(name, 0) {}
    // (3) a constructor with only a single argument duplicating
    // ALL of the above one but setting `value_{0}` in io
    // MI-List
    // (4) Like (3) but leaving out the initialization of `value_`
    // via its MI-list, using direct member initialization
    auto name() const { return name_; }
    auto value() const { return value_; }
    NamedValue& operator=(NamedValue& rhs) {
        value_ = rhs.value_;
            std::cout << "copy assign" << std::endl;
        return *this;
    }
    NamedValue& operator=(int value) { value_ = value; return *this; }
    NamedValue& operator+=(int value) { value_ += value; return *this; }
    NamedValue& operator-=(int value) { value_ -= value; return *this; }
    NamedValue& operator*=(int value) { value_ *= value; return *this; }
    NamedValue& operator/=(int value) { value_ /= value; return *this; }
    operator int() const { return value_; }

    static
    char const* strdup(char const* s) {
        return std::strcpy(new char[std::strlen(s)+1], s);
    }
};

#include <iostream>

std::ostream& operator<<(std::ostream& lhs, const NamedValue& rhs) {
    lhs << rhs.name() << ':' << ' ' << rhs.value();
    return lhs;
}

#endif
